/**
 * Created by hancock on 17-1-10.
 */
function checkBrowser() {
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;
    if (isIE || isEdge){
        alert("Please use Chrome or Firefox for better visualization!!!");
    }
}

/**
 * Created by hancock on 17-1-11.
 * draw network connecting genes and pathways
 */
function drawGenePath(selectedGenes,jsonFile, targetID) {
    var width = window.innerWidth;
    var height = window.innerHeight;
        var legendOffSetHeight = document.getElementById("navbarBox").offsetHeight + document.getElementById("select2Box").offsetHeight;
    console.log(legendOffSetHeight);

    d3.select("#testID").remove();
    var svg = d3.select('#'+targetID)
        .append("svg")
        .attr("id", "testID")
        .attr("width", width)
        .attr("height", height);

    var colors = d3.scaleOrdinal(d3.schemeCategory20);

    var legendTexts = ["Pathways in cancer",
        "Transcriptional misregulation in cancer",
        "Viral carcinogenesis",
        "Chemical carcinogenesis",
        "Proteoglycans in cancer",
        "MicroRNAs in cancer",
        "Central carbon metabolism in cancer",
        "Choline metabolism in cancer",
        "predicted"];


    var simulation = d3.forceSimulation()
        .force("link", d3.forceLink()
            .id(function(d) { return d.id; })
            .iterations(1))
        .force("collide", d3.forceCollide(function (d) {return 5;}))
        .force("charge", d3.forceManyBody()
            .strength(-500)
            .theta(0.9)
            .distanceMin(70)
            .distanceMax(100))
        .force("center", d3.forceCenter(width * 3 / 5, height * 4 / 10 ));

    d3.json(jsonFile, function(error, graph) {
        if (error) throw error;

        var remaining_nodes = [];
        var remaining_links = [];
        var related_pathways = [];
        var lookup_table = {};
        for (var i = 0; i < graph.nodes.length; i++){
            var node_id = graph.nodes[i].id;
            lookup_table[node_id] = i;
        }

        for (var i = 0; i < graph.nodes.length; i++){
            if ((selectedGenes.indexOf(graph.nodes[i].id) != -1)) {
                remaining_nodes.push(graph.nodes[i]);
            }
        }
        for (var i = 0; i < graph.links.length; i++){
            if ( selectedGenes.indexOf(graph.links[i].source) != -1) {
                remaining_links.push(graph.links[i]);
                var targetNodeIndex = lookup_table[graph.links[i].target];
                var targetNode = graph.nodes[targetNodeIndex];
                if (related_pathways.indexOf(targetNode) == -1)
                    related_pathways.push(targetNode);
            }
        }

        // add legends
        var illustrate_legends = svg.append("g")
            .attr("id", "entryLegends")
            .selectAll("rect")
            .data(legendTexts)
            .enter().append("rect")
            .attr("x", width / 40)
            .attr("y", function(d,i){
                return 40 * (i+ 1) + legendOffSetHeight;
            })
            .attr("width", width/25)
            .attr("height", 2.5)
            .attr("fill", function (d,i) {
                return colors(i);
            });

        // add legends texts
        var illustrate_legends = svg.append("g")
            .attr("id", "entryLegendsTexts")
            .selectAll("text")
            .data(legendTexts)
            .enter().append("text")
            .attr("x", width/40 + width/20)
            .attr("y", function(d,i){
                return 40 * (i+ 1) + legendOffSetHeight+ 5;
            })
            .text(function (d) {
                return  d
            });



        var link = svg.append("g")
            .attr("class", "links")
            .selectAll("line")
            .data(remaining_links)
            .enter().append("line")
            .attr("stroke", function (d,i) {
                return colors(d.value+3);
            })
            .attr("stroke-width", 1.5);

        var gene_nodes = svg.append("g")
            .attr("class", "genes")
            .selectAll("circle")
            .data(remaining_nodes)
            .enter().append("circle")
            .attr("id", "nodes")
            .attr("r", function (d) {
                return d.group * 2 + 3;
            })
            .attr("fill", function(d) { return colors(d.group); })
            .on("mouseover", function (d) {
                var xPosition = parseFloat(d3.mouse(this)[0]);
                var yPosition = parseFloat(d3.mouse(this)[1]);

                //Update the tooltip position and value
                var entityTooltip = d3.select("#tooltip");
                var tooltipOffsetHeight = document.getElementById("select2Box").offsetHeight +
                        document.getElementById("navbarBox").offsetHeight;
                entityTooltip.style("top", yPosition + tooltipOffsetHeight + "px")
                    .style("left", xPosition + "px");

                entityTooltip.select("#Description").text(function(){
                    return "hsa:" + d.description;
                });

                d3.select("#tooltip").classed("hidden", false);
            })
            .on("mouseout", function () {
                d3.select("#tooltip").classed("hidden", true);
            })
            .call(d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended));

        var pathway_nodes = svg.append("g")
            .attr("class", "pathways")
            .selectAll("image")
            .data(related_pathways)
            .enter()
            .append("a")
            .attr("id", "nodes")
            .attr("target", "_blank")
            .attr("xlink:href", function(d){
                // return "http://localhost:8000/demo/" + d.id + ".html";
                // return "http://139.210.167.206:52680/keggviewer/demo/" + d.id + ".html";
                //return "http://ml.jlu.edu.cn/gpvisual/keggviewer/demo/" + d.id + ".html";
                return "http://101.132.120.184/gpvisual/keggviewer/demo/" + d.id + ".html";
            })
            .append("image")
            .attr("xlink:href", "static/images/thumbnails/hsa05200.png")
            .attr("x", -50)
            .attr("y", -35)
            .attr("width", 100)
            .attr("height", 70)
            .on("mouseover", function (d) {
                var xPosition = parseFloat(d3.mouse(this)[0]);
                var yPosition = parseFloat(d3.mouse(this)[1]);

                //Update the tooltip position and value
                var entityTooltip = d3.select("#tooltip");


                var tooltipOffsetHeight = document.getElementById("select2Box").offsetHeight +
                    document.getElementById("navbarBox").offsetHeight;
                entityTooltip.style("top", d.y + tooltipOffsetHeight + "px")
                    .style("left", d.x + 50 +  "px")
                    .style("width", 200)
                    .classed("hidden", false);

                entityTooltip.select("#Description").text(function(){
                    return d.description;
                });

            })
            .on("mouseout", function () {
                d3.select("#tooltip").classed("hidden", true);
            })
            .call(d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended));


        simulation
            .nodes(graph.nodes)
            .on("tick", ticked);

        simulation.force("link")
            .links(graph.links);

        function ticked() {
            link
                .attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });

            gene_nodes
                .attr("cx", function(d) { return d.x; })
                .attr("cy", function(d) { return d.y; });
            pathway_nodes.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
        }
    });



    function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.2).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }

}
/**
 * Created by hancock on 17-1-10.
 * draw nets connecting genes, pubmed articles and pathwya
 */
function drawGenePubPath(jsonFile, targetID) {
    // var width = document.getElementById(targetID).Width;
    var width = window.innerWidth;
    var height = window.innerHeight;
    var svg = d3.select('#'+targetID)
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .on("mousedown",function(){
            d3.select("#tooltip").classed("hidden", true)
        });


    var colors = d3.scaleOrdinal(d3.schemeCategory20);

    var entityType = ['Gene', 'PubMed', "Pathway"];
    var legendTexts = ["Gene", "PubMed","Pathway",
        "Pathways in cancer",
        "Transcriptional misregulation in cancer",
        "Viral carcinogenesis",
        "Chemical carcinogenesis",
        "Proteoglycans in cancer",
        "MicroRNAs in cancer",
        "Central carbon metabolism in cancer",
        "Choline metabolism in cancer"];
    var pathwayIDs = ["05200","05202","05203","05204","05205","05206","05230","05231"];

    lengendToogle = true;
    svg.append("g")
        .attr("id", "helper")
        .append("text")
        .attr("font-family", "FontAwesome")
        .attr("x", width / 40 + width/50 - 25 / 2 )
        .attr("y", 30)
        .attr("font-size", "25")
        .text("\uf05a")
        .on("click",function () {
            svg.selectAll("#entityLegend")
                .classed("hidden", lengendToogle);
            svg.selectAll("#pathwayLegend")
                .classed("hidden", lengendToogle);
            svg.selectAll("#legendTexts")
                .classed("hidden", lengendToogle);
            lengendToogle = !lengendToogle;
            console.log(25/2);
        });

    svg.append("g")
        .attr("id","entityLegend")
        .selectAll("circle")
        .data(entityType)
        .enter()
        .append("circle")
        .attr("cx", width / 40 + width/50)
        .attr("cy", function (d,i) {
            return 20 + 40 * i + 20 + 10;
        })
        .attr("r", function (d,i) {
            return (i+1) * 2 + 3;
        } )
        .attr("fill", function (d,i) {
            return colors(i+1);
        });

    svg.append("g")
        .attr("id", "pathwayLegend")
        .selectAll("rect")
        .data(pathwayIDs)
        .enter()
        .append("rect")
        .attr("x", width / 40)
        // 140 = 20 + 40 * 3
        .attr("y", function(d,i){
            return 140+ 40 * i + 20 + 10;
        })
        .attr("width", width/25)
        .attr("height", 1.5)
        .attr("fill", function (d,i) {
            return colors(i+1+3)
        });

    svg.append("g")
        .attr("id","legendTexts")
        .selectAll("text")
        .data(legendTexts)
        .enter()
        .append("text")
        .attr("x", width * 1 / 40 + width/ 25 + 5)
        .attr("y", function (d,i) {
            return 20 + 40 * i + 25 + 10;
        })
        .text(function (d) {
            return d;
        });

    var simulation = d3.forceSimulation()
        .force("link", d3.forceLink()
            .id(function(d) { return d.id; })
            .iterations(1))
        .force("collide", d3.forceCollide(function (d) {return 20;}))
        .force("charge", d3.forceManyBody()
            .strength(10)
            .theta(0.6)
            .distanceMin(height/10)
            .distanceMax(height/3))
        .force("center", d3.forceCenter(width * 2 / 5, height * 4 / 10 ));

    d3.json(jsonFile, function(error, graph) {
        if (error) throw error;

        var link = svg.append("g")
            .attr("class", "links")
            .selectAll("line")
            .data(graph.links)
            .enter().append("line")
            .attr("stroke", function (d,i) {
                return colors(d.value+3);
            })
            .attr("stroke-width", 1.5);

        var node = svg.append("g")
            .attr("class", "nodes")
            .selectAll("circle")
            .data(graph.nodes)
            .enter().append("circle")
            .attr("r", function (d) {
                return d.group * 2 + 3;
            })
            .attr("fill", function(d) { return colors(d.group); })
            .on("mouseover", function(d) {
                var xPosition = parseFloat(d3.mouse(this)[0]);
                var yPosition = parseFloat(d3.mouse(this)[1]);

                //Update the tooltip position and value
                var entityTooltip = d3.select("#tooltip");

                entityTooltip.style("top", yPosition + 60 + "px")
                    .style("left", document.getElementById("illustration").offsetWidth + xPosition + "px")
                    .style("width", width/2 + "px");

                entityTooltip.select("#entityType")
                    .text(entityType[d.group-1]);
                entityTooltip.select("#entityID")
                    .text(d.id);
                entityTooltip.select("#entityName")
                    .text(function () {
                        if (d.group === 1) {
                            return "Common Names:" + d.entityName;
                        } else if (d.group === 2){
                            return  "Article Title:" + d.entityName;
                        } else {
                            return "Pathway Name:" + d.entityName;
                        }
                    });
                entityTooltip.select("#Description")
                    .text(function () {
                        if (d.group === 1) {
                            return "Gene Functions:" + d.description;
                        } else if (d.group === 2){
                            return  "Abstract:" + d.description;
                        } else {
                            return "Pathway Description:" + d.description;
                        }
                    });

                //Show the tooltip
                d3.select("#tooltip").classed("hidden", false);

            })
            .on("mouseout", function () {
                d3.select("#tooltip").classed("hidden", true);
            })
            .call(d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended))
            .on('dblclick', connectedNodes);

        // node.append("title")
        //     .text(function(d) { return d.id; });

        var toggle = 0;

//Create an array logging what is connected to what
        var linkedByIndex = {};
        graphLinks = graph.links;
        // console.log(graphLinks);
        // console.log(graphLinks[0])
        for (var i = 0; i < graphLinks.length; i++){
            linkedByIndex[graphLinks[i].source + "," + graphLinks[i].target] = 1;
        }

        function neighboring(a, b) {
            return linkedByIndex[a.id + "," + b.id];
        }


        simulation
            .nodes(graph.nodes)
            .on("tick", ticked);

        simulation.force("link")
            .links(graph.links);

        function ticked() {
            link
                .attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });

            node
                .attr("cx", function(d) { return d.x; })
                .attr("cy", function(d) { return d.y; });
        }


        function connectedNodes() {

            if (toggle == 0) {
                //Reduce the opacity of all but the neighbouring nodes
                d = d3.select(this).nodes()[0].__data__;
                // console.log(d);
                node.style("opacity", function (o) {
                    // console.log(o);
                    if (o !== d)
                        return neighboring(d, o) | neighboring(o, d) ? 1 : 0.1;
                    else return 1;
                });


                link.style("opacity", function (o) {
                    return d.index==o.source.index | d.index==o.target.index ? 1 : 0.1;
                });

                //Reduce the op

                toggle = 1;
            } else {
                //Put them back to opacity=1
                node.style("opacity", 1);
                link.style("opacity", 1);
                toggle = 0;
            }
            d3.select("#tooltip").classed("hidden", true);
        }
    });

    function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.2).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }

}

/**
 * Created by hancock on 17-1-9.
 */
function drawTable() {
    var x = 3;
    var y = 4;
}
/**
 * Created by hancock on 17-1-10.
 */
function refreshOnResize() {
    window.onresize = function(){
        window.location.href = window.location.href;
        location.reload(); };
    // document.getElementById("network").className="active";
}
